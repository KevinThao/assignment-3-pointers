
// Assignment 3 - Pointers
// <Your Name>


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function
void SwapIntegers(int *first, int *second)
{
	// put the first number somewhere else, so I can pull it back later (here, it is 'holdFirst')
	int holdFirst = *first;
	// swap first number to the second number, since I put the first number in 'holdFirst' I won't have to worry about it disappearing
	*first = *second;
	// this should swap the second number to the first number
	*second = holdFirst;
}

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}
